import eslintConfigAirbnbBase from 'eslint-config-airbnb-base';

export default {
  languageOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  plugins: { local: eslintConfigAirbnbBase },
  rules: {
    quotes: ['error', 'single'],
    semi: ['error', 'always'],
    indent: ['error', 2, { SwitchCase: 1 }],
    'max-len': ['error', { code: 120 }],
    'no-unused-vars': 'warn',
    'class-methods-use-this': 'off',
    'arrow-parens': 'off',
    'prefer-arrow-callback': 'off',
    'func-names': 'off',
    'object-curly-newline': 'off',
    'linebreak-style': 'off',
    'prefer-destructuring': 'off',
    'import/prefer-default-export': 'off',
    'import/extensions': 'off',
  },
};
